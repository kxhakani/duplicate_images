﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITesting;
using XnaFan.ImageComparison;

namespace ConsoleAppTestingImages
{
    class Program
    {
        static void Main(string[] args)
        {
            //string path1 = @"C:\DuplicateImages\1.jpg";
            //string path2 = @"C:\DuplicateImages\2.jpg";

            var sw = new Stopwatch();
            sw.Start();
            List<List<string>> dups = new List<List<string>>();

            Console.WriteLine("Starting...");

            //use this method to find the difference between two images (returns a float between 0 and 1)
            //float difference = (ImageTool.GetPercentageDifference(path1, path2));
            //Console.WriteLine(difference);


            //Use this method to get a list of duplicate images in a specific folder
            dups = ImageTool.GetDuplicateImages(@"C:\CheckForDuplicates", false);

            if (dups.Count == 0)
            {
                Console.WriteLine("No duplicates found");
            }
            else
            {
                Console.WriteLine(dups.Count + " duplicate images were found in total");
                Console.WriteLine("The following images are the duplicates: ");
            
                foreach (List<string> list in dups)
                {
                    foreach (string s in list)
                    {
                        Console.WriteLine(s);
                    }
                }
                
            }
            sw.Stop();
            TimeSpan ts = sw.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);

            Console.WriteLine();
            Console.WriteLine("Elapsed Time: " + elapsedTime);
            Console.WriteLine("Press ENTER to Exit ..... ");
            Console.ReadLine();
        }
    }
}
