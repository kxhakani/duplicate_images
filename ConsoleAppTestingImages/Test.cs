﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;
using XnaFan.ImageComparison;

namespace ConsoleAppTestingImages
{
    public class Test
    {

        public void ImageTest()
        {
            string path1 = @"C:\DuplicateImages\1.jpg";
            string path2 = @"C:\DuplicateImages\2.jpg";
            //use this method to find the difference between two images (returns a float between 0 and 1)
            int difference = (int)(ImageTool.GetPercentageDifference(path1, path2));


        }

    }
}
